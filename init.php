<?php
require __DIR__ . '/vendor/autoload.php';

foreach (['dictionaries', 'interfaces', 'services'] as $dirName) {
    $patch = __DIR__ . '/' . $dirName;
    foreach (scandir($patch) as $dir) {
        if (in_array($dir, ['.', '..'])) {
            continue;
        }

        require "$patch/$dir";
    }
}
