<?php

namespace interfaces;

/**
 * Interface ErrorLogServiceInterface
 *
 * @package interfaces
 */
interface ErrorLogServiceInterface
{
    /**
     * Writes data to file
     */
    public static function add(string $msg, int $typeError);
}