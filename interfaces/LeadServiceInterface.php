<?php

namespace interfaces;

use services\LeadServiceProcess;

/**
 * Interface LeadServiceInterface
 *
 * @package interfaces
 */
interface LeadServiceInterface
{
    public function run();
    public function save():LeadServiceProcess;
}