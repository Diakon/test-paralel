<?php
require __DIR__ . '/init.php';

use services\LeadServiceProcess;

set_time_limit(10 * 60); // 10 minutes for the script to work

echo "Start: " . date("Y-m-d H:i:s", time()) . "\n";

LeadServiceProcess::processLead();

echo "\nEnd: " . date("Y-m-d H:i:s", time());
echo "\nFile complete:" . LeadServiceProcess::getFile();