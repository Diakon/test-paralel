<?php

namespace dictionaries;

/**
 * Class LeadLogDictionary
 *
 * @package dictionaries
 */
class LeadLogDictionary
{
    const POOL_SIZE = 100;
    const LOG_FILE = __DIR__ . '/../log/log.txt';
}
