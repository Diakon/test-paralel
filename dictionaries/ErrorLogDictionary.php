<?php

namespace dictionaries;

/**
 * Class ErrorLogDictionary
 *
 * @package dictionaries
 */
class ErrorLogDictionary
{
    const TYPE_ERROR = 1;
    const TYPE_WARNING = 2;

    const TYPE_ERROR_FILE = [
        self::TYPE_ERROR => __DIR__ . '/../log/_error.log',
        self::TYPE_WARNING => __DIR__ . '/../log/_warning.log',
    ];
}
