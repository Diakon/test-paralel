<?php
namespace services;

use dictionaries\ErrorLogDictionary;
use interfaces\ErrorLogServiceInterface;

/**
 * Class ErrorLogService
 *
 * Save error to log
 */
class ErrorLogService implements ErrorLogServiceInterface
{
    /**
     * Add error to log
     *
     * @param string $msg
     * @param int $typeError
     */
    public static function add(string $msg, int $typeError = ErrorLogDictionary::TYPE_ERROR)
    {
        $patch = ErrorLogDictionary::TYPE_ERROR_FILE[$typeError] ?? ErrorLogDictionary::TYPE_ERROR;

        $msg = "[" . date("Y-m-d H:i:s", time()) . "] $msg\n";

        @file_put_contents($patch, $msg, FILE_APPEND);
    }
}