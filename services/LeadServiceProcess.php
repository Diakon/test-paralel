<?php
namespace services;

use LeadGenerator\Lead;
use dictionaries\LeadLogDictionary;
use interfaces\LeadServiceInterface;
use LeadGenerator\Generator;

/**
 * Class LeadService
 *
 * Service for working with leads
 *
 * @package services
 */
class LeadServiceProcess extends \Threaded implements LeadServiceInterface
{
    /**
     * @var int
     */
    public $id;
    /**
     * @var string
     */
    public $categoryName;
    /**
     * @var string
     */
    public $dateTime;

    /**
     * @param Lead $lead
     */
    public function __construct(Lead $lead)
    {
        $this->id = $lead->id;
        $this->categoryName = $lead->categoryName;
        $this->dateTime = date('Y-m-d H:i:s', time());
    }

    /**
     *
     */
    public function run()
    {
        sleep(2);
        $this->save();
    }

    /**
     * @param int $count
     */
    public static function processLead(int $count = 10000)
    {
        // Delete file if exist
        @unlink(self::getFile());

        $pool = new \Pool(LeadLogDictionary::POOL_SIZE);

        $generator = new Generator();
        $generator->generateLeads($count, function (Lead $lead) use ($pool) {
            $pool->submit(new LeadServiceProcess($lead));
        });
        while ($pool->collect());

        $pool->shutdown();
    }

    /**
     * Save to file
     *
     * @return $this
     */
    public function save():LeadServiceProcess
    {
        $leadData = [
            $this->id,
            $this->categoryName,
            $this->dateTime
        ];
        $file = self::getFile();
        try {
            file_put_contents($file, implode("|", $leadData) . "\n", FILE_APPEND);
            sleep(2);
        } catch (\Exception $e) {
            ErrorLogService::add("Error while writing data in file $file. Add info: " . json_encode($e->getTraceAsString()));
        }

        return $this;
    }

    /**
     * @return string
     */
    public static function getFile()
    {
        return LeadLogDictionary::LOG_FILE;
    }
}